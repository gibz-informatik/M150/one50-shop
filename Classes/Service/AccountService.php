<?php

namespace One50\Shop\Service;

use Neos\Flow\Annotations as Flow;
use Neos\Error\Messages\Message;
use Neos\Flow\Mvc\Controller\ActionController;
use Neos\Flow\ObjectManagement\DependencyInjection\DependencyProxy;
use Neos\Flow\Security\AccountRepository;
use Neos\Flow\Validation\ValidatorResolver;


/**
 * The account service provices multiple functionalities for creating and validating user accounts.
 */
class AccountService
{

    /**
     * @Flow\Inject
     * @var AccountRepository
     */
    protected $accountRepository;

    /**
     * @Flow\Inject
     * @var ValidatorResolver
     */
    protected $validatorResolver;

    /**
     * @Flow\Inject
     * @var \One50\Shop\I18n\Translator
     */
    protected $translator;

    /**
     * @param string           $username
     * @param ActionController $controller
     * @return array
     * @throws \Neos\Flow\Validation\Exception\InvalidValidationConfigurationException
     * @throws \Neos\Flow\Validation\Exception\NoSuchValidatorException
     */
    public function validateUsername(string $username): array
    {
        $flashMessages = [];

        if ($this->accountRepository instanceof DependencyProxy) {
            $this->accountRepository->_activateDependency();
        }

        $validators = [
            'One50.Shop:Unique' => ['repository' => $this->accountRepository, 'propertyName' => 'accountIdentifier'],
            'StringLength'      => ['minimum' => 5],
            'NotEmpty'          => []
        ];

        foreach ($validators as $validatorName => $validatorOptions) {
            $validator = $this->validatorResolver->createValidator($validatorName, $validatorOptions);
            $validationResult = $validator->validate($username);
            if ($validationResult->hasErrors()) {
                $flashMessages[] = [
                    $this->translator->translateById("username.validationFailed.{$validatorName}.body",
                        array($username)),
                    $this->translator->translateById("username.validationFailed.{$validatorName}.title",
                        array($username)),
                    Message::SEVERITY_ERROR
                ];
            }
        }

        return $flashMessages;
    }
}