<?php
namespace One50\Shop\Service;

use One50\Shop\Domain\Model\Order;

interface PaymentProviderInterface {
	
	const PAYMENT_STATUS_SUCCESS = 0;
	const PAYMENT_STATUS_IN_PROGRESS = 1;
	const PAYMENT_STATUS_FAILURE = 2;
	const PAYMENT_STATUS_UNKNOWN = 3;
	
	/**
	 * Start the payment process
	 *
	 * @param Order $order
	 */
	public function startPayment(Order $order);
	
	/**
	 * Returns the current payment status code for an order
	 * See class constants of this interface for status code interpretation.
	 *
	 * @param Order $order
	 * @return integer
	 */
	public function checkPaymentStatus(Order $order);
	
	
}