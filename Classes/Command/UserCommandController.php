<?php

namespace One50\Shop\Command;

/*
 * This file is part of the One50.Shop package.
 */

use Neos\Flow\Security\Account;
use Neos\Flow\Security\Policy\Role;
use One50\Shop\Domain\Model\User;
use Neos\Flow\Annotations as Flow;

/**
 * @Flow\Scope("singleton")
 */
class UserCommandController extends \Neos\Flow\Cli\CommandController
{

    /**
     * @var \Neos\Flow\Security\AccountFactory
     * @Flow\Inject
     */
    protected $accountFactory;

    /**
     * @var \Neos\Flow\Security\Cryptography\HashService
     * @Flow\Inject
     */
    protected $hashService;

    /**
     * Account Repository
     *
     * @var \Neos\Flow\Security\AccountRepository
     * @Flow\Inject
     */
    protected $accountRepository;

    /**
     * Person Repository
     *
     * @var \One50\Shop\Domain\Repository\UserRepository
     * @Flow\Inject
     */
    protected $userRepository;

    /**
     * Create an admin user
     *
     * Create a new user with basic information as username, password and full name.
     * Use this command to kickstart a new user with administrator role assigned.
     *
     * @param string $username  Username for account
     * @param string $password  Password for users account
     * @param string $firstName First name of user
     * @param string $lastName  Last name of user
     * @throws \Neos\Flow\Persistence\Exception\IllegalObjectTypeException
     */
    public function createAdminCommand(string $username, string $password, string $firstName, string $lastName)
    {
        $role = User::USER_ROLE_ADMIN;
        $this->createAccount($username, $password, $firstName, $lastName, $role);
        $this->outputLine('New user and account with admin role were successfully created.');
    }

    /**
     * Create a user
     *
     * Create a new user with basic information as username, password and full name.
     * Use this command to kickstart a new user with customer role assigned.
     *
     * @param string $username  Username for account
     * @param string $password  Password for account
     * @param string $firstName First name of user
     * @param string $lastName  Last name of user
     * @throws \Neos\Flow\Persistence\Exception\IllegalObjectTypeException
     */
    public function createUserCommand(string $username, string $password, string $firstName, string $lastName)
    {
        $role = User::USER_ROLE_CUSTOMER;
        $this->createAccount($username, $password, $firstName, $lastName, $role);
        $this->outputLine('New user and account with admin role were successfully created.');
    }

    /**
     * Here the actual creation of user and account happens.
     * The two commands "createAdminCommand" and "createUserComand" are alias methods for this method.
     *
     * @param string $username  Username for account
     * @param string $password  Password for account
     * @param string $firstName First name of user
     * @param string $lastName  Last name of user
     * @param string $role      Role to be assigned to account
     * @throws \Neos\Flow\Persistence\Exception\IllegalObjectTypeException
     */
    private function createAccount(
        string $username,
        string $password,
        string $firstName,
        string $lastName,
        string $role
    ) {
        $account = $this->accountFactory->createAccountWithPassword($username, $password, array($role));
        $this->accountRepository->add($account);

        $user = new User();
        $user->setFirstName($firstName);
        $user->setLastName($lastName);

        $user->addAccount($account);
        $this->userRepository->add($user);
    }

    /**
     * List all available accounts
     *
     * Prints a list with all available accounts to the console. For every account the account identifier, the roles an
     * the authentication provider name are printed.
     */
    public function listAccountsCommand()
    {
        $accounts = $this->accountRepository->findAll();
        if ($accounts->count() > 0) {
            $table = [];

            /** @var Account $account */
            foreach ($accounts as $account) {
                $table[] = [
                    $account->getAccountIdentifier(),
                    implode(', ', $account->getRoles()),
                    $account->getAuthenticationProviderName()
                ];
            }

            $this->output->outputTable($table, ['account identifier', 'roles', 'authentication provider']);
        } else {
            $this->outputLine("There are currently no accounts available!");
        }
    }

    /**
     * Reset users password
     *
     * Resets the passwort for the account identified by $username. The new password for this password must be
     * specified as second parameter.
     *
     * @param string $username    The account identifier
     * @param string $newPassword The new passwort to set
     * @throws \Neos\Flow\Persistence\Exception\IllegalObjectTypeException
     */
    public function resetPasswordCommand(string $username, string $newPassword)
    {
        $account = $this->accountRepository->findByAccountIdentifierAndAuthenticationProviderName($username,
            'DefaultProvider');

        if ($account instanceof Account) {
            $hashedPassword = $this->hashService->hashPassword($newPassword);
            $account->setCredentialsSource($hashedPassword);
            $this->accountRepository->update($account);
            $this->outputLine("Password for user '%s' was successfully updated!", [$username]);
        } else {
            $this->outputLine("No account with identifier '%s' exists.", [$username]);
        }
    }

}
