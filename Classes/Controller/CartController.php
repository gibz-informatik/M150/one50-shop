<?php
namespace One50\Shop\Controller;

/*
 * This file is part of the One50.Shop package.
 */

use One50\Shop\Domain\Model\Address;
use One50\Shop\Domain\Model\Order;
use One50\Shop\Domain\Model\Product;
use One50\Shop\Service\Cart;
use Neos\Flow\Annotations as Flow;
use Neos\Error\Messages\Message;
use Neos\Flow\Security\Account;

class CartController extends AbstractActionController {
	
	/**
	 * Translator
	 *
	 * @var \One50\Shop\I18n\Translator
	 * @Flow\Inject
	 */
	protected $translator;
	
	/**
	 * Security Context
	 *
	 * @var \Neos\Flow\Security\Context
	 * @Flow\Inject
	 */
	protected $securityContext;
	
	/**
	 * Party Service
	 *
	 * @var \Neos\Party\Domain\Service\PartyService
	 * @Flow\Inject
	 */
	protected $partyService;
	
	/**
	 * Cart
	 *
	 * @var Cart
	 * @Flow\Inject
	 */
	protected $cart;
	
	/**
	 * Order Repository
	 *
	 * @var \One50\Shop\Domain\Repository\OrderRepository
	 * @Flow\Inject
	 */
	protected $orderRepository;
	
	/**
	 * User Repository
	 *
	 * @var \One50\Shop\Domain\Repository\UserRepository
	 * @Flow\Inject
	 */
	protected $userRepository;
	
	/**
	 * @var array
	 */
	protected $settings;
	
	/**
	 * Inject the settings
	 *
	 * @param array $settings
	 * @return void
	 */
	public function injectSettings(array $settings) {
		$this->settings = $settings;
	}
	
	/**
	 * @return void
	 */
	public function indexAction() {
		$this->view->assign('cartItems', $this->cart->getItems());
	}
	
	/**
	 * Removes an item from the cart
	 *
	 * @param Product $product
	 */
	public function removeItemAction(Product $product) {
		$this->cart->removeItemByProduct($product);
		$this->redirect('index');
	}
	
	/**
	 * @param Address $billingAddress
	 * @param Address $deliveryAddress
	 */
	public function checkoutAction(Address $billingAddress = null, Address $deliveryAddress = null) {
		$this->view->assignMultiple(array('cartItems'       => $this->cart->getItems(),
										  'billingAddress'  => $billingAddress,
										  'deliveryAddress' => $deliveryAddress,
										  'paymentMethods'  => array_keys($this->settings['paymentMethods'])
									)
		);
	}
	
	/**
	 * Create a new address during checkout process
	 *
	 * @param Address $address
	 */
	public function createAddressAction(Address $address) {
		$user = $address->getUser();
		$user->addAddress($address);
		$this->userRepository->update($user);
		$this->redirect('checkout');
	}
	
	/**
	 * Initialize "placeOrderAction": Check wether billing/delivery addresses and payment method are set
	 */
	public function initializePlaceOrderAction() {
		$arguments = array();
		
		if (!$this->request->hasArgument('paymentMethod')) {
			$flashMessageBody = $this->translator->translateById('paymentMethod.required');
		} else {
			$arguments['paymentMethod'] = $this->request->hasArgument('paymentMethod');
		}
		
		if (!$this->request->hasArgument('billingAddress')) {
			$flashMessageBody = $this->translator->translateById('address.billing.required');
		} else {
			$arguments['billingAddress'] = $this->request->getArgument('billingAddress');
		}
		
		if (!$this->request->hasArgument('deliveryAddress')) {
			$flashMessageBody = $this->translator->translateById('address.delivery.required');
		} else {
			$arguments['deliveryAddress'] = $this->request->getArgument('deliveryAddress');
		}
		
		if (isset($flashMessageBody)) {
			$this->addFlashMessage(
				$flashMessageBody,
				'',
				Message::SEVERITY_ERROR
			);
			$this->redirect('checkout', null, null, $arguments);
		}
	}
	
	/**
	 * Persist an order
	 *
	 * @param Address $billingAddress
	 * @param Address $deliveryAddress
	 * @param string  $paymentMethod
	 */
	public function placeOrderAction(Address $billingAddress, Address $deliveryAddress, $paymentMethod) {
		$account = $this->securityContext->getAccount();
		$user = $this->partyService->getAssignedPartyOfAccount($account);
		
		$order = $this->cart->convertToOrder();
		$order->setUser($user);
		$order->setBillingAddress($billingAddress);
		$order->setDeliveryAddress($deliveryAddress);
		
		$this->cart->clear();
		$this->orderRepository->add($order);
		
		$paymentMethodConfiguration = $this->settings['paymentMethods'][$paymentMethod];
		$this->redirect('startPayment', $paymentMethodConfiguration['controller'], $paymentMethodConfiguration['packageKey'], array('order' => $order));
	}
	
}