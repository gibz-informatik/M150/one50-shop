<?php

namespace One50\Shop\Controller;

use Neos\Flow\Mvc\View\ViewInterface;

trait ShopViewPathsTrait
{

    /**
     * @param ViewInterface $view
     * @throws \Neos\Flow\Mvc\Exception
     */
    protected function addShopViewPaths(ViewInterface $view): void
    {
        /** @var \Neos\FluidAdaptor\View\TemplateView $view */
        /** @var string[] $layoutRootPaths */
        $layoutRoothPaths = [];
        $layoutRootPaths[] = "resource://One50.TwoFactorAuth/Private/Layouts";
        $layoutRootPaths[] = "resource://One50.Shop/Private/Layouts";
        array_push($layoutRoothPaths, ...$view->getRenderingContext()->getTemplatePaths()->getLayoutRootPaths());
        $view->setOption('layoutRootPaths', $layoutRootPaths);

        $templateRootPaths = [];
        $templateRootPaths[] = "resource://One50.TwoFactorAuth/Private/Templates";
        $templateRootPaths[] = "resource://One50.Shop/Private/Templates";
        array_push($templateRootPaths, ...$view->getRenderingContext()->getTemplatePaths()->getTemplateRootPaths());
        $view->setOption('templateRootPaths', $templateRootPaths);

        /** @var string[] $partialRootPaths */
        $partialRootPaths = [];
        $partialRootPaths[] = "resource://One50.TwoFactorAuth/Private/Partials";
        $partialRootPaths[] = "resource://One50.Shop/Private/Partials";
        array_push($partialRootPaths, ...$view->getRenderingContext()->getTemplatePaths()->getPartialRootPaths());
        $view->setOption('partialRootPaths', $partialRootPaths);
    }


}