<?php
namespace One50\Shop\Controller;

/*
 * This file is part of the One50.Shop package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use Neos\Flow\Mvc\View\ViewInterface;
use Neos\Flow\Security\Account;
use Neos\Flow\Security\Context;
use Neos\Party\Domain\Service\PartyService;
use One50\Shop\Domain\Repository\CategoryRepository;
use One50\Shop\Service\Cart;

abstract class AbstractActionController extends ActionController {

    use ShopViewPathsTrait;
	
	/**
	 * Shopping Cart
	 *
	 * @var Cart
	 * @Flow\Inject
	 */
	protected $cart;
	
	/**
	 * Security Context
	 *
	 * @var Context
	 * @Flow\Inject
	 */
	protected $securityContext;
	
	/**
	 * Party Service
	 *
	 * @var PartyService
	 * @Flow\Inject
	 */
	protected $partyService;
	
	/**
	 * Category Repository
	 *
	 * @var CategoryRepository
	 * @Flow\Inject
	 */
	protected $categoryRepository;
	
	/**
	 * User
	 *
	 * @var \One50\Shop\Domain\Model\User
	 */
	protected $user;

    /**
     * Assign the users cart items to the view
     *
     * @param ViewInterface $view
     * @throws \Neos\Flow\Mvc\Exception
     */
	protected function initializeView(ViewInterface $view) {
	    // Add shop view root paths
	    $this->addShopViewPaths($view);

		// assign categories to view
		$this->view->assign('categories', $this->categoryRepository->findAll());
		
		// assign cart to view
		$view->assign('cart', $this->cart);
		
		// assign user to view (if available)
		$account = $this->securityContext->getAccount();
		if ($account instanceof Account) {
			$this->user = $this->partyService->getAssignedPartyOfAccount($account);
			$this->view->assign('user', $this->user);
		}
	}
}
