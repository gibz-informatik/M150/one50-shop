<?php
namespace One50\Shop\Controller;

/*
 * This file is part of the One50.Shop package.
 */

use One50\Shop\Domain\Model\Order;
use Neos\Flow\Annotations as Flow;

class CashPaymentController extends \Neos\Flow\Mvc\Controller\ActionController {
	
	/**
	 * Translator
	 *
	 * @var \One50\Shop\I18n\Translator
	 * @Flow\Inject
	 */
	protected $translator;
	
	/**
	 * @return void
	 */
	public function startPaymentAction(Order $order) {
		$this->addFlashMessage(
			$this->translator->translateById('order.completed.0.info.1'),
			$this->translator->translateById('order.completed.0')
		);
		
		$this->redirect('show', 'Order', null, array('order' => $order));
	}
	
}
