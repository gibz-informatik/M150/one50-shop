<?php

namespace One50\Shop\Controller;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Security\Authentication\Controller\AbstractAuthenticationController;

class AuthenticationController extends AbstractAuthenticationController
{

    /**
     * @var \Neos\Party\Domain\Service\PartyService
     * @Flow\Inject
     */
    protected $partyService;

    /**
     * @var \One50\Shop\Service\Cart
     * @Flow\Inject
     */
    protected $cart;

    /**
     * @var \One50\Shop\Domain\Repository\CategoryRepository
     * @Flow\Inject
     */
    protected $categoryRepository;

    /**
     * @param \Neos\Flow\Mvc\View\ViewInterface $view
     */
    protected function initializeView(\Neos\Flow\Mvc\View\ViewInterface $view)
    {
        parent::initializeView($view);

        // assign categories to view
        $this->view->assign('categories', $this->categoryRepository->findAll());

        // assign cart to view
        $view->assign('cart', $this->cart);
    }

    /**
     * Display a form where the users may enter their login credentials
     *
     * @return void
     */
    public function indexAction()
    {
        // Just display the login form...
    }

    /**
     * Redirect a successfully authenticated user
     *
     * @param \Neos\Flow\Mvc\ActionRequest|null $originalRequest
     * @return string|void
     * @throws \Neos\Flow\Mvc\Exception\StopActionException
     */
    protected function onAuthenticationSuccess(\Neos\Flow\Mvc\ActionRequest $originalRequest = null)
    {
        if ($originalRequest !== null) {
            // redirect to original request, if there's one
            $this->redirectToRequest($originalRequest);
        } else {
            // redirect to users account instead
            $account = $this->securityContext->getAccount();
            /** @var \One50\Shop\Domain\Model\User $user */
            $user = $this->partyService->getAssignedPartyOfAccount($account);
            $this->redirect('show', 'User', null, array('user' => $user));
        }
    }

    /**
     * Logout the currently logged in user and redirect to the login form
     *
     * @throws \Neos\Flow\Mvc\Exception\StopActionException
     */
    public function logoutAction()
    {
        $this->authenticationManager->logout();
        $this->redirect('index');
    }

}