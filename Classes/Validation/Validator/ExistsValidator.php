<?php
namespace One50\Shop\Validation\Validator;

class ExistsValidator extends \Neos\Flow\Validation\Validator\AbstractValidator {
	
	/**
	 * @var array
	 */
	protected $supportedOptions = array(
		'repository'   => array(null, 'Repository to look for the unique property', 'Neos\Flow\Persistence\RepositoryInterface', true),
		'propertyName' => array(null, 'name of the unique property', 'string', true),
	);
	
	/**
	 * @param mixed $value The value that should be validated
	 * @return void
	 * @throws \Neos\Flow\Validation\Exception\InvalidValidationOptionsException
	 */
	protected function isValid($value) {
		$repository = $this->options['repository'];
		if (!$repository instanceof \Neos\Flow\Persistence\RepositoryInterface) {
			throw new \Neos\Flow\Validation\Exception\InvalidValidationOptionsException('The option "repository" must implement RepositoryInterface.', 1336499435);
		}
		
		$propertyName = (string)$this->options['propertyName'];
		$query = $repository->createQuery();
		$numberOfResults = $query->matching($query->equals($propertyName, $value))->count();
		if ($numberOfResults === 0) {
			$this->addError('This %s was not found', 1340810986, array($propertyName));
		}
	}
}

?>