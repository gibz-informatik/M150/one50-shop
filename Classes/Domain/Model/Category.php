<?php
namespace One50\Shop\Domain\Model;

/*
 * This file is part of the One50.Shop package.
 */

use Doctrine\Common\Collections\ArrayCollection;
use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Category {
	
	/**
	 * @var string
	 * @Flow\Validate(type="NotEmpty")
	 * @Flow\Validate(type="StringLength", options={"maximum"=120})
	 * @ORM\Column(length=120)
	 */
	protected $title;
	
	/**
	 * @var string
	 * @ORM\Column(length=1023)
	 */
	protected $description;
	
	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection<\One50\Shop\Domain\Model\Product>
	 * @ORM\OneToMany(mappedBy="category")
	 */
	protected $products;
	
	
	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}
	
	/**
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}
	
	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}
	
	/**
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}
	
	/**
	 * @return ArrayCollection
	 */
	public function getProducts() {
		return $this->products;
	}
	
	/**
	 * @param ArrayCollection $products
	 */
	public function setProducts($products) {
		$this->products = $products;
	}
	
	/**
	 * @param Product $product
	 */
	public function addProduct(Product $product) {
		$this->products->add($product);
	}
	
	/**
	 * @param Product $product
	 */
	public function removeProduct(Product $product) {
		$this->products->remove($product);
	}
}
