<?php
namespace One50\Shop\Domain\Model;

/*
 * This file is part of the One50.Shop package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Cart {
	
	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection<\One50\Shop\Domain\Model\CartItem>
	 * @ORM\OneToMany(mappedBy="cart")
	 */
	protected $items;
	
	/**
	 * @var \One50\Shop\Domain\Model\User
	 * @ORM\OneToOne
	 */
	protected $user;
	
	
	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection<\One50\Shop\Domain\Model\CartItem>
	 */
	public function getItems() {
		return $this->items;
	}
	
	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection<\One50\Shop\Domain\Model\CartItem> $items
	 * @return void
	 */
	public function setItems(\Doctrine\Common\Collections\ArrayCollection $items) {
		$this->items = $items;
	}
	
	/**
	 * @param OrderItem $item
	 * @return void
	 */
	public function addItem(OrderItem $item) {
		$this->items->add($item);
	}
	
	/**
	 * @param OrderItem $item
	 * @return void
	 */
	public function removeItem(OrderItem $item) {
		$this->items->remove($item);
	}
	
	/**
	 * @return \One50\Shop\Domain\Model\User
	 */
	public function getUser() {
		return $this->user;
	}
	
	/**
	 * @param \One50\Shop\Domain\Model\User $user
	 * @return void
	 */
	public function setUser(\One50\Shop\Domain\Model\User $user) {
		$this->user = $user;
	}
	
}
