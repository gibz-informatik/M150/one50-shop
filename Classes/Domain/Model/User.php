<?php
namespace One50\Shop\Domain\Model;

/*
 * This file is part of the One50.Shop package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;
use Neos\Party\Domain\Model\AbstractParty;

/**
 * @Flow\Entity
 */
class User extends AbstractParty {
	
	const USER_ROLE_CUSTOMER = 'One50.Shop:Customer';
	const USER_ROLE_ADMIN = 'One50.Shop:Admin';
	
	/**
	 * @var string
	 * @Flow\Validate(type="NotEmpty")
	 * @Flow\Validate(type="StringLength", options={"maximum"=80})
	 * @ORM\Column(length=80)
	 */
	protected $firstName;
	
	/**
	 * @var string
	 * @Flow\Validate(type="NotEmpty")
	 * @Flow\Validate(type="StringLength", options={"maximum"=80})
	 * @ORM\Column(length=80)
	 */
	protected $lastName;
	
	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection<\One50\Shop\Domain\Model\Address>
	 * @ORM\OneToMany(mappedBy="user")
	 */
	protected $addresses;
	
	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection<\One50\Shop\Domain\Model\Order>
	 * @ORM\OneToMany(mappedBy="user")
	 */
	protected $orders;
	
	/**
	 * @return string
	 */
	public function getFirstName() {
		return $this->firstName;
	}
	
	/**
	 * @param string $firstName
	 */
	public function setFirstName($firstName) {
		$this->firstName = $firstName;
	}
	
	/**
	 * @return string
	 */
	public function getLastName() {
		return $this->lastName;
	}
	
	/**
	 * @param string $lastName
	 */
	public function setLastName($lastName) {
		$this->lastName = $lastName;
	}
	
	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getAddresses() {
		return $this->addresses;
	}
	
	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection $addresses
	 */
	public function setAddresses($addresses) {
		$this->addresses = $addresses;
	}
	
	/**
	 * @param Address $address
	 */
	public function addAddress(Address $address) {
		$this->addresses->add($address);
	}
	
	/**
	 * @param Address $address
	 */
	public function removeAddress(Address $address) {
		$this->addresses->remove($address);
	}
	
	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getOrders() {
		return $this->orders;
	}
	
	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection $orders
	 */
	public function setOrders($orders) {
		$this->orders = $orders;
	}
	
	/**
	 * @param Order $order
	 */
	public function addOrder(Order $order) {
		$this->orders->add($order);
	}
	
	/**
	 * @param Order $order
	 */
	public function removeOrder(Order $order) {
		$this->orders->remove($order);
	}
	
}
