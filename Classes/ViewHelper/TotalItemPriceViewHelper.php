<?php
namespace One50\Shop\ViewHelper;


use One50\Shop\Domain\Model\CartOrderItemInterface;
use Neos\FluidAdaptor\Core\ViewHelper\AbstractViewHelper;

class TotalItemPriceViewHelper extends AbstractViewHelper {
	
	/**
	 * Returns the subtotal for this cart/order item
	 *
	 * @param CartOrderItemInterface $item
	 * @return int
	 */
	public function render(CartOrderItemInterface $item) {
		return $item->getProduct()->getPrice() * $item->getQuantity();
	}
}