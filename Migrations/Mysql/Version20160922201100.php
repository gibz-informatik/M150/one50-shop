<?php
namespace Neos\Flow\Persistence\Doctrine\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs! This block will be used as the migration description if getDescription() is not used.
 */
class Version20160922201100 extends AbstractMigration
{

    /**
     * @return string
     */
    public function getDescription()
    {
        return '';
    }

    /**
     * @param Schema $schema
     * @return void
     */
    public function up(Schema $schema)
    {
        // this up() migration is autogenerated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on "mysql".');
        
        $this->addSql('DROP TABLE one50_shop_domain_model_product_images_join');
        $this->addSql('ALTER TABLE one50_shop_domain_model_product ADD image VARCHAR(40) DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E306649BC53D045F ON one50_shop_domain_model_product (image)');
    }

    /**
     * @param Schema $schema
     * @return void
     */
    public function down(Schema $schema)
    {
        // this down() migration is autogenerated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on "mysql".');
        
        $this->addSql('CREATE TABLE one50_shop_domain_model_product_images_join (shop_product VARCHAR(40) NOT NULL COLLATE utf8_unicode_ci, flow_resource_resource VARCHAR(40) NOT NULL COLLATE utf8_unicode_ci, INDEX IDX_BF375517D0794487 (shop_product), INDEX IDX_BF37551714E3D2D7 (flow_resource_resource), PRIMARY KEY(shop_product, flow_resource_resource)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE one50_shop_domain_model_product_images_join ADD CONSTRAINT FK_BF375517D0794487 FOREIGN KEY (shop_product) REFERENCES one50_shop_domain_model_product (persistence_object_identifier)');
        $this->addSql('DROP INDEX UNIQ_E306649BC53D045F ON one50_shop_domain_model_product');
        $this->addSql('ALTER TABLE one50_shop_domain_model_product DROP image');
    }
}